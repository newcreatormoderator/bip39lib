package bip39lib;

func IsEqualSym(chars, word []string) bool {
    for j := 0; j < len(chars); j++ {
	if chars[j] != word[j]{
	    return false
	}
    }
    return true
}

func SameSymbols(chars, word []string) bool {
    var wordpart []string 
    var  int
    for i := 0; i < len(word)-len(chars);  i++ {
	wordpart = word[i:len(chars)]
	if !IsEqualSym(chars, wordpart){
	    return false
	}
    }
    return true
}

func GetWordsWithChars(char string, wordlist []string) []string {
    var arrchar, arrword []string
    arrchar = strings.Split(char, "")
    for i := 0; i < len(wordlist); i++ {
	arrword = strings.Split(wordlist[i], "")
	if SameSymbols(arrchar, arrword) {
	    searched = append(searched, wordlist[i])
	}
    }
    return searched
}

func randr(numbers []int, num, count int) int{
    for i:=0; i < len(numbers); i++ {
	if num == numbers[i]{
	    num = rand.Intn(count)
	    i = 0;
	}
    }
    return num
}

func OpenLangWordsf(lang string) []string{
    wordsreaded, err := ioutil.ReadFile("./"+lang+".txt")
    if err != nil {
	log.Fatal("Error when opening file: ", err)
    }
    bufwords := strconv.Itoa(int(wordsreaded))
    wordsarray := strings.Split(bufwords, string(85))
    return wordsarray
}

func MakeList(wordscount int, language string) []string {
    var randomNum int
    var phrase []string
    var choosednums []int
    rand.Seed(time.Now().UnixNano())
    AlWords := OpenLangWordsf(language)
    for i := 0; i < wordscount; i++ {
	randNum = randr(choosednums, rand.Intn(len(AlWords-1)), len(AlWords-1))
	phrase = append(phrase, AllLangWords[randNum])
	choosednums = append(choosednums, randNum)
    }
    return phrase
}

func ArrayExist(stringslice []string, elem string) bool {
    for i:=0; i < len(stringslice); i++ {
	if elem == stringslice[i]{
	    return true
	}
    }
    return false
}

func TranslateList(phrase []string, languagefrom, languageto string) []string {
    var numstranslation []int
    var newphrase []string
    vocalbularyfrom := Openlangwordsf(languagefrom)
    for i := 0; i < len(phrase); i++ {
	if ArrayExist(phrase, vocalbularyfrom[i]){
            numstranslation = append(numstranslation, i)
	}
    }
    vocalbularyto := Openlangwordsf(languageto)
    for i := 0; i < len(numstranslation); i++ {
	newphrase = append(newphrase, vocalbularyto[numstranslation[i]])
    }
    return newphrase 
}

func GetIDofwords(phrase []string, language string) []int {
    var numstranslation []int
    vocalbulary := Openlangwordsf(languagefrom)
    for i := 0; i < len(phrase); i++ {
	if ArrayExist(phrase, vocalbularyfrom[i]){
            numstranslation = append(numstranslation, i)
	}
    }
    return numstranslation
}

func GetListLanguageAndLength(phrase []string, lang string) (string, int){
    var language []string
    var wordscount int
    var langwords []string
    for i := 0; i < len(phrase); i++{
	langwords = Openlangwordsf(lang)
	if ArrayExist(langwords, phrase[i]){
	    if !ArrayExist(language, langwords){
		language = append(language, langwords)
		wordscount = append(wordscount, 0)
	    }
	    wordscount += 1
	}
    }
    return language, wordscount
}


func ListValidity(phrase []string, language string) bool{
    vocalbulary := OpenLangWordsf(language)
    for i := 0; i < len(phrase); i++ {
	if ArrayExist(vocalbulary, phrase[i]){
	    break
	}
	if (i == len(phrase)-1) && !ArrayExist(vocalbulary, phrase[i]) {
	    return false
	}
    }
    return true
}

func RemoveSymbols(text string) string{
    regex, err := regexp.Compile(`[^\w]`)
    if err != nil {
	log.Fatal(err)
    }
    text = regex.ReplaceAllString(str1, " ")
    return text
}

func MakeListFromText(text string, language string, count int) []string{
    var list []string
    text = RemoveSymbols(text)
    wordsfromtext := strings.Split(text, " ")
    vocalbulary := OpenLangWordsf(language)
    for i:= 0, j:=0; j < count; i++{
	if ArrayExist(vocalbulary, wordsfromtext[i]){
	    list = append(list, wordsfromtext[i])
	    j++
	}
    }
    return wordsfromtext
}

func Comparewords(langone, langtwo []string) []string{
    var newword string
    for i:=0; i<len(langone); i++ {
	if ArrayExist(langtwo, langone[i]){
	    fmt.Printf("%s, exists into both vocalbularies, write new word: ")
	    fmt.Scanf("%s" ,&newword)
	    langone[i] = newword
	}
    }
    return langone
}

func RepeatValidation(langs []string, langidx int) []string{
    var vocalbularysec string
    vocalbularyfirst := OpenLangWordsf(langs[langidx])
    for i:=langidx+1; i < len(langs); i++ {
	vocalbularysec = OpenLangWordsf(langs[i])
	vocalbularyfirst = Comparewords(vocalbularyfirst, vocalbularysec)
    }
    return RepeatValidation(langs, langidx+1)
}
